-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 04, 2021 at 05:11 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `sightings`
--

CREATE TABLE `sightings` (
  `idSighting` int(11) NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `dateSighting` datetime NOT NULL,
  `description` longtext NOT NULL,
  `registrationDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sightings`
--

INSERT INTO `sightings` (`idSighting`, `latitude`, `longitude`, `dateSighting`, `description`, `registrationDate`) VALUES
(1, '12.109378743085693', '-86.27304443225073', '2021-08-26 23:20:39', 'Data about the sighting of the sasquatch', '2021-08-27 23:20:39'),
(2, '12.134659362883168', '-86.26936144574199', '2021-08-25 23:20:39', 'testing2', '2021-08-27 23:20:39'),
(3, '13.109378743085692', '-86.27304443225073', '2021-08-26 00:20:39', 'testing3', '2021-08-26 00:20:39'),
(5, '12.109378743085692', '-96.27304443225073', '2021-08-26 00:20:39', 'testing4', '2021-08-26 00:20:39'),
(6, '11.109378743085692', '-96.27304443225073', '2021-08-30 00:20:39', 'testing5', '2021-08-30 00:20:39'),
(7, '11.109378743085692', '-96.27304443225073', '2021-08-30 00:20:39', 'testing5', '2021-08-30 00:20:39'),
(8, '11.109378743085692', '-96.27304443225073', '2021-08-30 00:20:39', 'testing6', '2021-08-30 00:20:39'),
(9, '11.109378743085692', '-96.27304443225073', '2021-08-30 00:20:39', 'testing6', '2021-08-30 00:20:39');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `idTag` int(11) NOT NULL,
  `tag` varchar(50) NOT NULL,
  `registrationDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`idTag`, `tag`, `registrationDate`) VALUES
(1, 'hill', '2021-08-27 23:17:02'),
(2, 'dark-brown', '2021-08-27 23:17:02'),
(3, 'cabbage-patch', '2021-08-27 23:18:11'),
(4, 'tree', '2021-08-26 00:20:39'),
(5, 'mountain', '2021-08-26 00:20:39'),
(6, 'free', '2021-08-26 00:20:39'),
(7, 'river', '2021-08-26 00:20:39'),
(8, 'street', '2021-08-26 00:20:39'),
(19, 'mountain', '2021-08-31 08:07:41'),
(20, 'dirty', '2021-08-31 08:07:41'),
(21, 'river', '2021-09-03 10:24:40'),
(22, 'rock', '2021-09-03 10:24:40'),
(23, 'river', '2021-09-04 03:41:50'),
(24, 'hill', '2021-09-04 03:41:50'),
(25, 'river', '2021-09-04 03:43:53'),
(26, 'rock', '2021-09-04 03:43:53'),
(27, 'rock', '2021-09-04 03:44:35');

-- --------------------------------------------------------

--
-- Table structure for table `tag_details`
--

CREATE TABLE `tag_details` (
  `idTag` int(11) NOT NULL,
  `idSighting` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tag_details`
--

INSERT INTO `tag_details` (`idTag`, `idSighting`) VALUES
(7, 5),
(8, 5),
(7, 7),
(8, 7),
(7, 8),
(8, 8),
(7, 9),
(8, 9),
(25, 1),
(27, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sightings`
--
ALTER TABLE `sightings`
  ADD PRIMARY KEY (`idSighting`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`idTag`);

--
-- Indexes for table `tag_details`
--
ALTER TABLE `tag_details`
  ADD KEY `idSighting` (`idSighting`),
  ADD KEY `idTag` (`idTag`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sightings`
--
ALTER TABLE `sightings`
  MODIFY `idSighting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `idTag` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tag_details`
--
ALTER TABLE `tag_details`
  ADD CONSTRAINT `tag_details_ibfk_1` FOREIGN KEY (`idSighting`) REFERENCES `sightings` (`idSighting`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tag_details_ibfk_2` FOREIGN KEY (`idTag`) REFERENCES `tags` (`idTag`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
