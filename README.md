# README #

Sasquatch sightings

### What is this repository for? ###

The "Society to Uncover and Spread the Truth" has hired you to build a backend to track Sasquatch sightings with an API to access this data. They need a system to track sightings, including some metadata about each sighting, and to provide some analytics to help them discover patterns.

* Version 1.0

### How do I get set up? ###

* STEP 1
    - Install Xampp or Install PHP 7.4, Apache and Mysql 5.7
* STEP 2
    - You can test the  API REST with Postman, I'll add a file .json to import on postman with all the collection that I Created.
    - Database information
        - DB Name: test
        - Host: localhost
        - Username: root
        - password:
    - I add the test.sql in Dir "resources". to import the file to mysql
