<?php
    include "config.php";
    include "utils.php";
    $dbConn =  connect($db);

    if ($_SERVER['REQUEST_METHOD'] == 'GET')
    {
        if (isset($_GET['idA']) && isset($_GET['idB']))
        {
            //Data of Point A
            $sql = $dbConn->prepare("SELECT latitude, longitude FROM sightings where idSighting=:idA");
            $sql->bindValue(':idA', $_GET['idA']);
            $sql->execute();
            $point1 = $sql->fetch();

            //Data of Point B
            $sql = $dbConn->prepare("SELECT latitude, longitude FROM sightings where idSighting=:idB");
            $sql->bindValue(':idB', $_GET['idB']);
            $sql->execute();
            header("HTTP/1.1 200 OK");
            $point2 = $sql->fetch();

            $distance = distanceCalculation($point1['latitude'], $point1['longitude'], $point2['latitude'], $point2['longitude']);
            $dataDistance = array(
                'Location A' => [
                    'Latitude' => $point1['latitude'],
                    'Longitude' => $point1['longitude']
                ], 
                'Location B' => [
                    'Latitude' => $point2['latitude'],
                    'Longitude' => $point2['longitude']
                ], 
                'Distance Between LocationA to LocationB' => $distance.'Km'
            );
            echo json_encode($dataDistance);
            exit();
	    }
        else {
            //empty value
            header("HTTP/1.1 400 OK");
            echo "Error";
            exit();
	    }
}
?>