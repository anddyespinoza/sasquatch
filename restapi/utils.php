<?php
//Open database conecction
function connect($db)
{
	try {
		$conn = new PDO("mysql:host={$db['host']};dbname={$db['db']};charset=utf8", $db['username'], $db['password']);
		// set the PDO error mode to exception
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $conn;
	} catch (PDOException $exception) {
		exit($exception->getMessage());
	}
}
//get params to update
function getParams($input)
{
	$filterParams = [];
	foreach ($input as $param => $value) {
		$filterParams[] = "$param=:$param";
	}
	return implode(", ", $filterParams);
}
//Associate all parameters with a sql
function bindAllValues($statement, $params)
{
	foreach ($params as $param => $value) {
		$statement->bindValue(':' . $param, $value);
	}
	return $statement;
}
//Get all the params of tags to insert data
function getTagsParams($input)
{
	if (isset($input['tags'])) {
		$dataTags['registrationDate'] = $input['registrationDate'];
		return $dataTags;
	}
}
//Get all the params of sighting to insert data
function getSightingParams($input)
{
	if (isset($input['tags'])) {
		unset($input['tags']);
		return $input;
	}
}

function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $decimals = 2) 
{
	//Calculate the distance between to points with the formulae of haversine
	$degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));
	// formulae to convert distance in grade to Kilometers 
	$distance = $degrees * 111.13384; // 1 garde = 111.13384 km, based on the average diameter of the Earth	(12.735 km)

	return round($distance, $decimals);
}