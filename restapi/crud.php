<?php
include "config.php";
include "utils.php";
$dbConn =  connect($db);
/*

*/
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	if (isset($_GET['idSighting'])) {
		//Get a Sighting by idSighting
		$sql = $dbConn->prepare("SELECT * FROM sightings INNER JOIN tag_details ON tag_details.idSighting = sightings.idSighting INNER JOIN tags ON tags.idTag = tag_details.idTag WHERE sightings.idSighting =:idSighting");
		$sql->bindValue(':idSighting', $_GET['idSighting']);
		$sql->execute();
		header("HTTP/1.1 200 OK");
		echo json_encode($sql->fetchAll(PDO::FETCH_ASSOC));
		exit();
	} else {
		//Get all the Sighting
		$sql = $dbConn->prepare("SELECT idSighting, dateSighting FROM sightings");
		$sql->execute();
		$sql->setFetchMode(PDO::FETCH_ASSOC);
		header("HTTP/1.1 200 OK");
		$results = $sql->fetchAll(PDO::FETCH_ASSOC);
		echo json_encode($results);
		exit();
	}
}



// Crear un nuevo post
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$input = $_POST;
	$dataSighting = getSightingParams($input);			//I Create a funtion to get only the Sighting's params
	$insertSighting = "INSERT INTO sightings (latitude, longitude, dateSighting, description, registrationDate) VALUES (:latitude, :longitude, :dateSighting, :description, :registrationDate)";
	$statement = $dbConn->prepare($insertSighting);
	bindAllValues($statement, $dataSighting);
	$statement->execute();
	$sightingId = $dbConn->lastInsertId();
	if ($sightingId) {									//If the insert was good the next step is insert the info of th tags in the others tables
		$dataTagDetails['idSighting'] = $sightingId;	//I take the id of the last Sighting's insert

		//If the variable Tags exist we go to insert the info
		if (isset($_POST['tags'])) {
			$dataTags = getTagsParams($input);			//I create a funtion to separate the variables tags of the main variable to only insert the correct information
			$tags = explode(",", str_replace(' ', '', $_POST['tags']));

			//Create a cicle to insert the tags one by one in the tables
			for ($i = 0; $i < count($tags); $i++) {
				//I check if the tag exist or not
				$dataTags['tag'] = $tags[$i];
				$checkDataTag = $dbConn->prepare("SELECT idTag FROM tags where tag=:tag");
				$checkDataTag->bindValue(':tag', $dataTags['tag']);
				$checkDataTag->execute();
				$numberOfData = $checkDataTag->rowCount();
				//If the tag doesn't Exist i going to insert the new tag
				if ($numberOfData <= 0) {
					$insertTags = "INSERT INTO tags (tag, registrationDate) VALUES (:tag, :registrationDate)";
					$statement = $dbConn->prepare($insertTags);
					bindAllValues($statement, $dataTags);
					$statement->execute();
					$tagId = $dbConn->lastInsertId();
					//If the tag was insert correct i take the id of this tag and i insert to the table tagDetails with the sighting ID
					if (isset($tagId)) {
						$dataTagDetails['idTag'] = $tagId;
						$insertTagDetail = "INSERT INTO tag_details (idTag, idSighting) VALUES (:idTag, :idSighting)";
						$statement = $dbConn->prepare($insertTagDetail);
						bindAllValues($statement, $dataTagDetails);
						$statement->execute();
					}
				}
				//If the Tag already exist i take de Id of the tag and i insert the information to tag_details
				else {
					$tagId = $checkDataTag->fetch(PDO::FETCH_ASSOC);
					$dataTagDetails['idTag'] = $tagId['idTag'];
					$insertTagDetail = "INSERT INTO tag_details (idTag, idSighting) VALUES (:idTag, :idSighting)";
					$statement = $dbConn->prepare($insertTagDetail);
					bindAllValues($statement, $dataTagDetails);
					$statement->execute();
				}
			}
		}
		header("HTTP/1.1 201 OK");
		exit();
	}
}


//Actualizar
if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
	//Validate if ubication or descrition's params exist 
	if (isset($_GET['idSighting']) && isset($_GET['latitude']) && isset($_GET['longitude']) || isset($_GET['description'])) {
		$sightingData = getSightingParams($_GET);	//I catch all the params of the table sightings
		$sightingId = $sightingData['idSighting'];
		$fields = getParams($sightingData);
		$sql = "UPDATE sightings SET $fields WHERE idSighting='$sightingId'";
		$statement = $dbConn->prepare($sql);
		bindAllValues($statement, $sightingData);
		$statement->execute();
		header("HTTP/1.1 200 OK");
		exit();

	} elseif (isset($_GET['tags']) && isset($_GET['idSighting'])) {
		//Else if tags exist and idSighting we use this to update or delate the tags
		$tagSavedInfo = $dbConn->prepare("SELECT tags.*, sightings.idSighting FROM sightings INNER JOIN tag_details ON tag_details.idSighting = sightings.idSighting INNER JOIN tags ON tags.idTag = tag_details.idTag WHERE sightings.idSighting=:idSighting");
		$tagSavedInfo->bindValue(':idSighting', $_GET['idSighting']);
		$tagSavedInfo->execute();
		$totalTags = str_replace(' ', '', $_GET['tags']);			// I use str_replace to delete any space of the list
		$newTagList = explode(",", $totalTags);						// With Explode I create a array with the List of the tags
		$mainLists = $tagSavedInfo->fetchAll(PDO::FETCH_ASSOC);
		foreach ($mainLists as $mainList) {							// This First loop is to check all the tags of the table 
			for ($i = 0; $i < count($newTagList); $i++) {			// This cicle is to explore each tag supplied for the user
				if ($mainList['tag'] == $newTagList[$i]) {			// Compare the Main list of tags with the new list if there is a match I erase the tag from the new list of tags
					unset($newTagList[$i]);							// I use unset to erase the tag from the array
					break;
				} else {
					//If there is not math between the new list with all the tags that mean that the user erased the tag and I erase the conecction  in the table tag_details
					$deleteTagDetails = $dbConn->prepare("DELETE FROM tag_details where idTag=:idTag AND idSighting=:idSighting");		
					$deleteTagDetails->bindValue(':idTag', $mainList['idTag']);
					$deleteTagDetails->bindValue(':idSighting', $_GET['idSighting']);
					$deleteTagDetails->execute();
				}
			}
		}
		// in the end I insert the tags that were left in the new list like new tags
		foreach ($newTagList as $tagList) {
			$dataTags['tag'] = $tagList;
			$dataTags['registrationDate'] = date("Y-m-d h:i:s");
			$insertTags = "INSERT INTO tags (tag, registrationDate) VALUES (:tag, :registrationDate)";
			$statement = $dbConn->prepare($insertTags);
			bindAllValues($statement, $dataTags);
			$statement->execute();
			$tagId = $dbConn->lastInsertId();
			//If the tag was insert correct i take the id of this tag and i insert to the table tagDetails with the sighting ID
			if (isset($tagId)) {
				$dataTagDetails['idTag'] = $tagId;
				$dataTagDetails['idSighting'] = $_GET['idSighting'];
				$insertTagDetail = "INSERT INTO tag_details (idTag, idSighting) VALUES (:idTag, :idSighting)";
				$statement = $dbConn->prepare($insertTagDetail);
				bindAllValues($statement, $dataTagDetails);
				$statement->execute();
			}
		}
		header("HTTP/1.1 200 OK");
		exit();
	} else {
		echo "Missing Data.";
	}
}


//Detale a Sighting 
if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
	$idSighting = $_GET['idSighting'];
	$deleteSighting = $dbConn->prepare("DELETE FROM sightings where idSighting=:idSighting");
	$deleteSighting->bindValue(':idSighting', $idSighting);
	$deleteSighting->execute();
	$deleteTagDetails = $dbConn->prepare("DELETE FROM tag_details where idSighting=:idSighting");
	$deleteTagDetails->bindValue(':idSighting', $idSighting);
	$deleteTagDetails->execute();
	header("HTTP/1.1 200 OK");
	exit();
}

//En caso de que ninguna de las opciones anteriores se haya ejecutado
header("HTTP/1.1 400 Bad Request");
