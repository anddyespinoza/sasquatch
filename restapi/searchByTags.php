<?php
include "config.php";
include "utils.php";
$dbConn =  connect($db);
/*
Get all the Sighting
*/
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	//Get All sightings that match all the the tags
	if (isset($_GET['allTags']) AND $_GET['allTags'] == 'TRUE') {
		$tags = explode(",", str_replace(' ', '', $_GET['tags']));
		//I concatenate the variable $query to insert the list of tag to search
		$query = "SELECT s.* FROM ( SELECT td.idSighting FROM tag_details AS td JOIN tags t ON t.idTag = td.idTag WHERE t.tag IN (";		
		for ($i = 0; $i < count($tags); $i++) {
			if (!empty($tags[$i])) {
				$query .= "'".$tags[$i]."',";
			}
		}
		$query = substr($query, 0, -1);		// I erased the last caracter is a "," with this the code break
		$query .= ") GROUP BY td.idSighting HAVING COUNT(1) = ".$i." ) AS search JOIN sightings AS s ON s.idSighting = search.idSighting";
		$sql = $dbConn->prepare($query);
		$sql->execute();
		header("HTTP/1.1 200 OK");
		echo json_encode($sql->fetchAll(PDO::FETCH_ASSOC));
		exit();
		//GET All sightings that match at least one of the tags in the search request
	} elseif (isset($_GET['allTags']) AND $_GET['allTags'] == 'FALSE') {	
		$tags = explode(",", str_replace(' ', '', $_GET['tags']));
		
		$query = "SELECT s.* FROM sightings AS s INNER JOIN tag_details AS td ON td.idSighting = s.idSighting INNER JOIN tags AS t ON t.idTag = td.idTag WHERE tag LIKE '".$tags[0]."'";
		for ($i = 1; $i < count($tags); $i++) {
			if (!empty($tags[$i])) {
				$query .= " OR tag LIKE '" . $tags[$i] . "'";
			}
		}
		$query .= " GROUP BY s.idSighting";
		$sql = $dbConn->prepare($query);
		$sql->execute();
		header("HTTP/1.1 200 OK");
		echo json_encode($sql->fetchAll(PDO::FETCH_ASSOC));
		exit();
	}
}
